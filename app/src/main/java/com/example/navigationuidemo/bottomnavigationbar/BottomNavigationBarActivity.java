package com.example.navigationuidemo.bottomnavigationbar;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.navigationuidemo.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

/**
 * @author: njb
 * @date: 2020/9/15 0015 0:47
 * @desc:
 */
public class BottomNavigationBarActivity extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_navigation_bar);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        //设置底部菜单
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation_view);
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                Toast.makeText(BottomNavigationBarActivity.this, "onDestinationChanged() called", Toast.LENGTH_SHORT).show();
            }
        });

        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.bottomNavigationBarMainFragment, R.id.bottomNavigationBarBroadcastFragment, R.id.bottomNavigationBarSettingsFragment)
                .build();
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(bottomNavigationView, navController);
    }
}
