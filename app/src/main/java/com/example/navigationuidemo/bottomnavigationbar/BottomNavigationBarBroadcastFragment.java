package com.example.navigationuidemo.bottomnavigationbar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.example.navigationuidemo.R;

/**
 * @author: njb
 * @date: 2020/9/15 0015 0:52
 * @desc:
 */
public class BottomNavigationBarBroadcastFragment extends Fragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bottom_navigation_bar_broadcast, container, false);
    }
}
